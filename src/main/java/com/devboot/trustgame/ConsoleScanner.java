package com.devboot.trustgame;

public class ConsoleScanner implements ScanInput {

    @Override
    public String getInput(){
        java.util.Scanner scanner = new java.util.Scanner(System.in);
        String input = null;
        input = scanner.nextLine();
        return input;
    }

}