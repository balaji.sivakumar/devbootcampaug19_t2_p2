package com.devboot.trustgame;

public interface PlayerBehavior {
    PlayerBehavior CHEAT_PLAYER_BEHAVIOR = () -> "CH";
    PlayerBehavior COOPERATE_PLAYER_BEHAVIOR = () -> "CO";

    String move();
}
