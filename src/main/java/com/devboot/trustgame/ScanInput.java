package com.devboot.trustgame;

public interface ScanInput {
    String getInput();
}
