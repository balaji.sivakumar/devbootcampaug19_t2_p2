package com.devboot.trustgame;

public class ConsolePlayerBehavior implements PlayerBehavior {
    private ScanInput scanner;

    public ConsolePlayerBehavior(ScanInput scanner) {
        this.scanner = scanner;
    }

    @Override
    public String move() {
        System.out.println("Enter Input: Cheat (CH)/ Co-Operate (CO)");
        return this.scanner.getInput();
    }
}