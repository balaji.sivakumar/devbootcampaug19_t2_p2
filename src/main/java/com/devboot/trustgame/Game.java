package com.devboot.trustgame;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class Game extends Observable {

    private final Player player1;

    private final Player player2;

    private final Machine machine;

    private final int noOfRounds;

    private final PrintStream console;

    public Game(Player player1, Player player2, Machine machine, int noOfRounds, PrintStream console) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfRounds = noOfRounds;
        this.console = console;
    }

    public void play() {
        int[] results;
        for (int i = 0; i < noOfRounds; i++) {
            String player1Move = player1.move();
            String player2Move = player2.move();
            results = machine.calculateScore(player1Move, player2Move);
            player1.updateScore(results[0]);
            player2.updateScore(results[1]);
            Map<Integer, String> playerIdsAndTheirMoves = new HashMap<>();
            playerIdsAndTheirMoves.put(player1.getId(), player1Move);
            playerIdsAndTheirMoves.put(player2.getId(), player2Move);
            this.setChanged();
            this.notifyObservers(new Notification(playerIdsAndTheirMoves));
            console.println(player1.getScore() + "," + player2.getScore());
        }
    }

}
