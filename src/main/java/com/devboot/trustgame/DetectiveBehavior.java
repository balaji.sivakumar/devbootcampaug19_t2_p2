package com.devboot.trustgame;

import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class DetectiveBehavior implements PlayerBehavior, Observer {

    private final int opponentPlayerIdToObserve;
    private PlayerBehavior behaviorToImitate;
    private static final List<String> FIRST_4_MOVES = Arrays.asList("CO", "CH", "CO", "CO");
    private int moveCount = 0;
    private boolean opponentCheatedAtLeastOnce = false;

    public DetectiveBehavior(int opponentPlayerIdToObserve) {
        this.opponentPlayerIdToObserve = opponentPlayerIdToObserve;
    }

    @Override
    public String move() {
        moveCount++;
        if (moveCount <= 4) {
            return FIRST_4_MOVES.get(moveCount - 1);
        } else {
            return this.behaviorToImitate.move();
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Notification notification = (Notification) arg;
        String opponentMove = notification.getPlayerIdsAndTheirMoves().get(opponentPlayerIdToObserve);
        if (moveCount <= 4) {
            if (opponentMove.equals("CH")) {
                this.opponentCheatedAtLeastOnce = true;
                if (this.behaviorToImitate == null) {
                    this.behaviorToImitate = PlayerBehavior.CHEAT_PLAYER_BEHAVIOR;
                    o.deleteObserver(this);
                }
            }
        } else {//moveCount is >= 5
            if (!this.opponentCheatedAtLeastOnce) {
                this.behaviorToImitate = new CopyCatBehavior(opponentPlayerIdToObserve);
            }
        }
    }
}
