package com.devboot.trustgame;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MachineTest {

    private Machine machine;

    @Before
    public void setup(){
        this.machine = new Machine();
    }

    @Test
    public void shouldCreateMachineInstance(){
        Machine machine = new Machine();
    }

    @Test
    public void shouldCalculateScoreForP1CooperateAndP2Cheat(){
        int[] result= machine.calculateScore("CO","CH");
        Assert.assertArrayEquals(new int[] {-1,3}, result);
    }
    @Test
    public void shouldCalculateScoreForP1CooperateAndP2Cooperate(){
        int[] result= machine.calculateScore("CO","CO");
        Assert.assertArrayEquals(new int[] {2,2}, result);
    }
    @Test
    public void shouldCalculateScoreForP1CheatAndP2Cheat(){
        int[] result= machine.calculateScore("CH","CH");
        Assert.assertArrayEquals(new int[] {0,0}, result);
    }
}
