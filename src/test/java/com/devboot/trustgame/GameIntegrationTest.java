package com.devboot.trustgame;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class GameIntegrationTest {


    private Player cheatingPlayer;
    private Player cooperatingPlayer;
    private Machine machine;
    private int noOfRounds;
    private Game game;
    private PrintStream console;

    @Before
    public void setUp() {
        cheatingPlayer = new Player(PlayerBehavior.CHEAT_PLAYER_BEHAVIOR, 1);
        cooperatingPlayer = new Player(PlayerBehavior.COOPERATE_PLAYER_BEHAVIOR, 2);
        machine = new Machine();
        noOfRounds = 5;
        console = mock(PrintStream.class);

        game = new Game(cheatingPlayer, cooperatingPlayer, machine, noOfRounds, console);
    }

    @Test
    public void shouldPlayOneRoundBetweenCheaterAndCooperatingPlayers() {
        game.play();

        Assert.assertNotNull(game);
        Assert.assertEquals(15, cheatingPlayer.getScore());
        Assert.assertEquals(-5, cooperatingPlayer.getScore());
    }

    @Test
    public void shouldPlayMultipleRoundBetweenCheaterAndCooperatingPlayers() {
        game.play();

        Assert.assertNotNull(game);
        Assert.assertEquals(15, cheatingPlayer.getScore());
        Assert.assertEquals(-5, cooperatingPlayer.getScore());
    }

    @Test
    public void shouldPlayMultipleRoundsWithCoopAndGrudgePlayer() {
        Player coop = new Player(PlayerBehavior.COOPERATE_PLAYER_BEHAVIOR, 1);
        GrudgeBehavior grudgeBehavior = new GrudgeBehavior(1);
        Player grudger = new Player(grudgeBehavior, 2);
        game = new Game(coop, grudger, machine, noOfRounds, console);
        game.addObserver(grudgeBehavior);
        game.play();
        Assert.assertEquals(10, coop.getScore());
        Assert.assertEquals(10, grudger.getScore());
    }

    @Test
    public void shouldPlayMultipleRoundsWithCopyCatAndGrudgePlayer() {
        CopyCatBehavior copyBehavior = new CopyCatBehavior(2);
        Player copyCat = new Player(copyBehavior, 1);
        GrudgeBehavior grudgeBehavior = new GrudgeBehavior(1);
        Player grudger = new Player(grudgeBehavior, 2);
        game = new Game(copyCat, grudger, machine, noOfRounds, console);
        game.addObserver(copyBehavior);
        game.addObserver(grudgeBehavior);
        game.play();
        Assert.assertEquals(10, copyCat.getScore());
        Assert.assertEquals(10, grudger.getScore());
    }

    @Test
    public void shouldPlayMultipleRoundsWithCheaterAndGrudger() {
        Player cheat = new Player(PlayerBehavior.CHEAT_PLAYER_BEHAVIOR, 1);
        GrudgeBehavior grudgeBehavior = new GrudgeBehavior(1);
        Player grudger = new Player(grudgeBehavior, 2);
        PrintStream console = mock(PrintStream.class);
        game = new Game(cheat, grudger, machine, noOfRounds, console);
        game.addObserver(grudgeBehavior);
        game.play();
        verify(console, times(5)).println("3,-1");

        Assert.assertEquals(3, cheat.getScore());
        Assert.assertEquals(-1, grudger.getScore());
    }

    @Test
    public void shouldPlayMultipleRoundsWithCheaterAndDetective() {
        Player cheat = new Player(PlayerBehavior.CHEAT_PLAYER_BEHAVIOR, 1);
        DetectiveBehavior detectiveBehavior = new DetectiveBehavior(1);
        Player detective = new Player(detectiveBehavior, 2);
        PrintStream console = mock(PrintStream.class);
        game = new Game(cheat, detective, machine, 5, console);
        game.addObserver(detectiveBehavior);
        game.play();
        verify(console, times(2)).println("3,-1");
        verify(console, times(1)).println("6,-2");
        verify(console, times(2)).println("9,-3");
        Assert.assertEquals(9, cheat.getScore());
        Assert.assertEquals(-3, detective.getScore());
    }

    @Test
    public void shouldPlayMultipleRoundsWithDetectiveAndCopyCat() {
        DetectiveBehavior detectiveBehavior = new DetectiveBehavior(2);
        Player detective = new Player(detectiveBehavior, 1);
        CopyCatBehavior copyCatBehavior = new CopyCatBehavior(1);
        Player copyCatPlayer = new Player(copyCatBehavior, 2);
        PrintStream console = mock(PrintStream.class);
        game = new Game(detective, copyCatPlayer, machine, 6, console);
        game.addObserver(detectiveBehavior);
        game.addObserver(copyCatBehavior);
        game.play();
        verify(console, times(1)).println("2,2");
        verify(console, times(1)).println("5,1");
        verify(console, times(1)).println("4,4");
        verify(console, times(1)).println("6,6");
        verify(console, times(2)).println("9,5");
        Assert.assertEquals(9, detective.getScore());
        Assert.assertEquals(5, copyCatPlayer.getScore());
    }

}
