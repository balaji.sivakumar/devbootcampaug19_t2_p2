package com.devboot.trustgame;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PlayerTest {

    private Player player1;

    private MockScanner scanner1;

    private Player player2;

    private MockScanner scanner2;

    @Before
    public void setup(){
        this.scanner1 = new MockScanner();
        this.player1 = new Player(new ConsolePlayerBehavior(scanner1),1);
        this.scanner2 = new MockScanner();
        this.player2 = new Player(new ConsolePlayerBehavior(scanner2),2);
    }

    @Test
    public void shouldReturnPlayerInstance(){
        Assert.assertNotNull(player1);
        Assert.assertNotNull(player2);
    }

    @Test
    public void shouldMakeMoveForPlayer1CheatAndPlayer2Cheat(){
        this.scanner1.setMoveInput("CH");
        this.scanner2.setMoveInput("CH");
        String move1 = this.player1.move();
        String move2 = this.player2.move();
        Assert.assertEquals("CH",move1);
        Assert.assertEquals("CH",move2);
    }

    @Test
    public void shouldMakeMoveForPlayer1CooperateAndPlayer2Cheat(){
        this.scanner1.setMoveInput("CO");
        this.scanner2.setMoveInput("CH");
        String move1 = this.player1.move();
        String move2 = this.player2.move();
        Assert.assertEquals("CO",move1);
        Assert.assertEquals("CH",move2);
    }

    @Test
    public void shouldMakeMoveForPlayer1CheatAndPlayer2Cooperate(){
        this.scanner1.setMoveInput("CH");
        this.scanner2.setMoveInput("CO");
        String move1 = this.player1.move();
        String move2 = this.player2.move();
        Assert.assertEquals("CH",move1);
        Assert.assertEquals("CO",move2);
    }
}
